package com.sunbeaminfo.sh.main;

import java.util.List;

import com.sunbeaminfo.sh.daos.BookDao;
import com.sunbeaminfo.sh.entities.Book;
import com.sunbeaminfo.sh.util.JpaUtil;

public class Hb08Main {
	public static void main(String[] args) {
		/*
		try(BookDao dao = new BookDao()) {
			dao.open();
 			Book b = dao.getBook(11);
 			System.out.println(b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
		
		/*
		try(BookDao dao = new BookDao()) {
			dao.open();
 			List<Book> list = dao.getBookBySubject("C");
 			for (Book b : list)
 				System.out.println(b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
		
		/*
		try(BookDao dao = new BookDao()) {
			dao.open();
 			Book b = new Book(51, "Sherlock Homes", "Unknown", "Novell", 434.23);
 			dao.addBook(b);
 			System.out.println(b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
		
		/*
		try(BookDao dao = new BookDao()) {
			dao.open();
 			Book b = dao.getBook(51);
 			b.setPrice(823.34);
 			dao.updateBook(b);
 			System.out.println(b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
		
		/*
		try(BookDao dao = new BookDao()) {
			dao.open();
 			dao.deleteBook(51);
 			System.out.println("Deleted.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
	
		/*
		try(BookDao dao = new BookDao()) {
			dao.open();
 			double price = dao.getBookPrice(11);
 			System.out.println("Book Price : " + price);
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
		
		try(BookDao dao = new BookDao()) {
			dao.open();
 			List<Book> list = dao.getBooksByAuthor("Kanetkar");
 			for (Book b : list)
 				System.out.println(b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		JpaUtil.shutdown();
	}
}
